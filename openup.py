from flask import Flask, render_template

app = Flask(__name__,static_url_path='/static')

#@app.route('/')
#def webprint():
#    return render_template('index.html')

@app.route('/')
@app.route('/users')
def users():
    with open("/Users/richardsjo/WTP/WTP_Website/static/WTP/Kanto.txt") as g:
        kanto = [line.strip() for line in g]
    with open("/Users/richardsjo/WTP/WTP_Website/static/WTP/Johto.txt") as h:
        johto = [line.strip() for line in h]
    with open("/Users/richardsjo/WTP/WTP_Website/static/WTP/Hoenn.txt") as i:
        hoenn = [line.strip() for line in i]
    with open("/Users/richardsjo/WTP/WTP_Website/static/WTP/Sinnoh.txt") as j:
        sinnoh = [line.strip() for line in j]
    with open("/Users/richardsjo/WTP/WTP_Website/static/WTP/Unova.txt") as k:
        unova = [line.strip() for line in k]
    with open("/Users/richardsjo/WTP/WTP_Website/static/WTP/Kalos.txt") as l:
        kalos = [line.strip() for line in l]
    with open("/Users/richardsjo/WTP/WTP_Website/static/WTP/Alola.txt") as m:
        alola = [line.strip() for line in m]
    return render_template('index.html',kanto=kanto,johto=johto,hoenn=hoenn,sinnoh=sinnoh,unova=unova,kalos=kalos,alola=alola)
if __name__ == '__main__':
    app.run(debug=True)
