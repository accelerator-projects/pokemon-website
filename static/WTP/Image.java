import java.awt.Color;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.image.WritableRaster;
import java.util.Random;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.*;
import java.lang.reflect.Array;
import java.util.Arrays;
public class Image
{
	public static String[] create(String genNum) throws Exception
	{
		if (genNum.equals("1"))
		{
			Scanner in = new Scanner(new File("Kanto.txt"));
			List<String> outputList = new ArrayList<>();
			String input = null;
			while (in.hasNextLine() && null != (input = in.nextLine()))
			{
			outputList.add(input);
			}
			String[] outputArray = new String[outputList.size()];
			String[] pokedex = outputList.toArray(outputArray);
			in.close();
			return pokedex;
		}
		else if (genNum.equals("2"))
		{
			Scanner in = new Scanner(new File("Johto.txt"));
			List<String> outputList = new ArrayList<>();
			String input = null;
			while (in.hasNextLine() && null != (input = in.nextLine()))
			{
			outputList.add(input);
			}
			String[] outputArray = new String[outputList.size()];
			String[] pokedex = outputList.toArray(outputArray);
			in.close();
			return pokedex;
		}
		else if (genNum.equals("3"))
		{
			Scanner in = new Scanner(new File("Hoenn.txt"));
			List<String> outputList = new ArrayList<>();
			String input = null;
			while (in.hasNextLine() && null != (input = in.nextLine()))
			{
			outputList.add(input);
			}
			String[] outputArray = new String[outputList.size()];
			String[] pokedex = outputList.toArray(outputArray);
			in.close();
			return pokedex;
		}
		else if (genNum.equals("4"))
		{
			Scanner in = new Scanner(new File("Sinnoh.txt"));
			List<String> outputList = new ArrayList<>();
			String input = null;
			while (in.hasNextLine() && null != (input = in.nextLine()))
			{
			outputList.add(input);
			}
			String[] outputArray = new String[outputList.size()];
			String[] pokedex = outputList.toArray(outputArray);
			in.close();
			return pokedex;
		}
		else if (genNum.equals("5"))
		{
			Scanner in = new Scanner(new File("Unova.txt"));
			List<String> outputList = new ArrayList<>();
			String input = null;
			while (in.hasNextLine() && null != (input = in.nextLine()))
			{
			outputList.add(input);
			}
			String[] outputArray = new String[outputList.size()];
			String[] pokedex = outputList.toArray(outputArray);
			in.close();
			return pokedex;
		}
		else if (genNum.equals("6"))
		{
			Scanner in = new Scanner(new File("Kalos.txt"));
			List<String> outputList = new ArrayList<>();
			String input = null;
			while (in.hasNextLine() && null != (input = in.nextLine()))
			{
			outputList.add(input);
			}
			String[] outputArray = new String[outputList.size()];
			String[] pokedex = outputList.toArray(outputArray);
			in.close();
			return pokedex;
		}
		else if (genNum.equals("7"))
		{
			Scanner in = new Scanner(new File("Alola.txt"));
			List<String> outputList = new ArrayList<>();
			String input = null;
			while (in.hasNextLine() && null != (input = in.nextLine()))
			{
			outputList.add(input);
			}
			String[] outputArray = new String[outputList.size()];
			String[] pokedex = outputList.toArray(outputArray);
			in.close();
			return pokedex;
		}
		else
		{
			Scanner in = new Scanner(new File("Pokemon.txt"));
			List<String> outputList = new ArrayList<>();
			String input = null;
			while (in.hasNextLine() && null != (input = in.nextLine()))
			{
			System.out.println(input);
			outputList.add(input);
			}
			String[] outputArray = new String[outputList.size()];
			String[] pokedex = outputList.toArray(outputArray);
			in.close();
			return pokedex;
		}
	}
	public static String random(String[] pokedex, int genNum)
	{
		Random gen = new Random();
		System.out.println(Arrays.toString(pokedex));
		String poke = pokedex[gen.nextInt(genNum)];
		return poke;
	}
	public static void save(String name, BufferedImage image) throws Exception
	{
		File saveAs = new File(name + "_new.png");
		ImageIO.write(image, "png", saveAs);
	}

	public static void darken(String name)
	{
		try
		{
		BufferedImage image = ImageIO.read(new File(name + ".png"));
		int width = image.getWidth();
		int height = image.getHeight();
		WritableRaster raster = image.getRaster();
		for (int col = 0; col < width; col++)
		{
			for (int row = 0; row < height; row++)
			{
				int[] pixels = raster.getPixel(col, row, (int[]) null);
				pixels[0] = 0;
				pixels[1] = 0;
				pixels[2] = 0;
				raster.setPixel(col, row, pixels);
			}
		}
		Image.save(name,image);
		}
		catch(Exception e)
		{
			System.out.println(name);
		}
	}

	public static <T> T[] joinArrayGeneric(T[]... arrays) {
        int length = 0;
        for (T[] array : arrays) {
            length += array.length;
        }

        //T[] result = new T[length];
        final T[] result = (T[]) Array.newInstance(arrays[0].getClass().getComponentType(), length);

        int offset = 0;
        for (T[] array : arrays) {
            System.arraycopy(array, 0, result, offset, array.length);
            offset += array.length;
        }

        return result;
    }
}
